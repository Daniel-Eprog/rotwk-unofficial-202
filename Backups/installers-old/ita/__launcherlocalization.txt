Translate only the right part of each line, right side of the equal symbol (the values). 
Don't alter the text left of the equal symbols (the keys).
Respect the \n secquences. Sometimes a \n secquence is contiguous to translateable text, 
like in "[...]release.\nThey[...]".
In those cases translate the words but leave the \n in the same place. 
For example, I would translate that into "[...]versión.\nEllos[...]"
This file must be kept in Unicode encoding, otherwise the international characters are not going to be shown.
Every line that has not an equal symbol in it is ignored and so they won't be used or shown.
If the program is showing a "MISSING:whatever" instead of translated text, that's because you have modified the left part
of a line (the one containing the key 'whatever'), or you removed that line completely.


dcmissingfiles = Errore durante l'accesso ai file della Patch DC installati nel tuo sistema. \nDettagli: 
u202missingfiles = Errore durante l'accesso ai file dell'Unofficial 2.02 installati nel tuo sistema. \nDettagli: 
outdateddcfiles = Uno o più file della Patch DC sono stati trovati disattivati da una versione precedente.\nEssi sono stati cancellati.
outdatedu202files = Uno o più file dell'Unofficial 2.02 sono stati trovati disattivati da una versione precedente.\nEssi sono stati cancellati.
dcenabled = La mod Patch DC è attiva.
u202enabled = La mod Unofficial 2.02 è attiva.
dcdisabled = La mod Patch DC non è attiva.
u202disabled = La mod Unofficial 2.02 non è attiva.
dcinconsistent = La mod Patch DC è attiva in modo incoerente. Attivala o Disattivala. Altrimenti il gioco non funzionerà correttamente.
u202inconsistent = La mod Unofficial 2.02 è attiva in modo incoerente. Attivalo o Disattivalo. Altrimenti il gioco non funzionerà correttamente.
u202removeold = Trovati vecchi file della 2.02.\nDovresti cancellare questi file, altrimenti il launcher potrebbe non attivare o disattivare correttamente la patch U2.02.\nVuoi cancellare i file disabilitati?

helpbutton = Aiuto
errormustreinstall = Uno o più file essenziali della 2.02 risultano mancanti. Reinstallare la patch seguendo attentamente le istruzioni di installazione.\nVuoi andare alla pagina di download?
installerror = Installazione non corretta

u202repairask = Sono stati trovati file attivi e file non attivi dell'U2.02. I file non attivi appartengono probabilmente ad una versione obsoleta.\nDovresti cancellare quelli non attivi, altrimenti il launcher potrebbe fallire nell'attivare o nel disattivare la patch U2.02.\nVuoi cancellare i file non attivi? (Raccomandato)
warningtitle = ATTENZIONE!
installedversion = Versione installata: 
lookingforlatestversion = Sto cercando la versione più recente ...
errortouchingdcfiles = Si è verificato un errore durante l'uso dei file Director's Cut!\nAssicurati di eseguire il Patch Launcher come amministratore.
errortouchingu202files = Si è verificato un errore durante l'uso dei file Director's Cut!\nAssicurati di eseguire il Patch Launcher come amministratore.
errorreinstallgame = Il gioco non è installato correttamente. Il gioco dovrà essere installato o reinstallato per poter usare questo programma.
wrongresvalues = Si prega di scrivere una coppia di numeri valida separati da uno spazio vuoto o deselezionare la casella di controllo della risoluzione.
wrongresvaluestitle = Valore della risoluzione non valido
latestversion = Versione più recente: 
dcoutdated = La tua versione della Patch DC è obsoleta!
u202outdated = La tua versione dell'Unofficial 2.02 è obsoleta!
dcnew = Esiste una nuova versione della patch Director's Cut: 
u202new = Esiste una nuova versione dell'Unofficial 2.02: 
dcuptodate = La tua versione della Patch DC è aggiornata.
u202uptodate = La tua versione dell'Unofficial 2.02 è aggiornata.
downloadpageask = Vuoi andare alla pagina di download?
newversiontitle = Esiste una nuova versione!
usingunreleased = Stai usando una versione non rilasciata (probabilmente provvisoria)!
cannotfindversionserver = Impossibile trovare online l'ultima versione.

modstatelabel = La mod è attualmente ...
enable202label = Attiva Unofficial 2.02
enabledclabel = Attiva Director's Cut
noversioninfo = Non ci sono informazioni sulla versione.
resolutionlabel = Risoluzione personalizzata
videoslabel = Mostra i filmati di introduzione
debuglabel = Esegui il gioco in modalità script debug
compatibilitylabel = Modalità di compatibilità vecchi replay
musiclabel = Musica della Lobby
musicgood = Tema del Bene
musicevil = Tema del Male
musiccombined = Tema del Bene e del Male
musicstandard = Musica della Lobby Standard
changelogbutton = Mostra le Modifiche
startbutton = Avvia il Gioco
exitbutton = Esci
windowtitle = Patch Launcher
changelogtitle = Modifiche
changelogclosebutton = Chiudi

multiplayer = Ottimizza per Multigiocatore Online
