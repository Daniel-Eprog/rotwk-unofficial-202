/*
 * main.cpp
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

// For GUI
#include <QApplication>
#include <QWidget>

#include "OldNew.h"


int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	OldNew window(0, "No game.dat active!\nTry pressing new/old.");

	window.resize(300, 120);
	window.setWindowTitle("RotWK game.dat switcher");
	window.show();

	return app.exec();
}
