/*
 * OldNew.cpp
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "OldNew.h"
#include <QGridLayout>

#define OLD_SUM "84659690d2b47506bb23b2b43bd3b238"
#define NEW_SUM "fbff091f6139eb05dc012d834e3eeb74"

#define OLD_SUM_SHA "bf52bf0b7430afcf1cc823fd58943ce830919d41"
#define NEW_SUM_SHA "37326974a91724b8a98ed6c7841c20c9b7be2ad7"

#define OLD_SIZE 12204384
#define NEW_SIZE 11313184

// MegaBytes
#define MB *1000000

/**
 * Calculates an md5 checksum from a file.
 *
 * Will return the string "Error" if something went wrong
 *
 * Mapping:  QString -> QString
 */
QString fileMD5(QString file_name)
{
	const char * output = "Error";

	QFile f(file_name);
	QCryptographicHash hash(QCryptographicHash::Md5);

	if (f.open(QFile::ReadOnly))
	{
		hash.addData(f.readAll());

		output = hash.result().toHex();
	}

	return QString(output);
}

/**
 * Calculates an sha1 checksum from a file.
 *
 * Will return the string "Error" if something went wrong
 *
 * Mapping:  QString -> QString
 */
QString fileSHA1(QString file_name)
{
	const char * output = "Error";

	QFile f(file_name);
	QCryptographicHash hash(QCryptographicHash::Sha1);

	if (f.open(QFile::ReadOnly))
	{
		hash.addData(f.readAll());

		output = hash.result().toHex();
	}

	return QString(output);
}



OldNew::OldNew(QWidget *parent, QString labelName)
    : QWidget(parent) {

	QPushButton *newBtn = new QPushButton("new", this);
	QPushButton *oldBtn = new QPushButton("old", this);
	lbl = new QLabel(labelName, this);

	if (!(fileSHA1("game.dat").compare(NEW_SUM_SHA)))
		lbl->setText("New game.dat\n\nActive!");

	if (!(fileSHA1("game.dat").compare(OLD_SUM_SHA)))
		lbl->setText("Old game.dat\n\nActive!");

	lbl->setAlignment(Qt::AlignCenter);


	QGridLayout *grid = new QGridLayout(this);
	grid->addWidget(newBtn, 0, 0);
	grid->addWidget(oldBtn, 0, 3);
	grid->addWidget(lbl, 1, 2);

	setLayout(grid);

	connect(newBtn, SIGNAL(clicked()), this, SLOT(ActNew()));
	connect(oldBtn, SIGNAL(clicked()), this, SLOT(ActOld()));

}

/**
 * Scans directory "." for new and old game.dat files (checksum based)
 * 
 * If anything is found, the following members are updated: 
 * - gamedatold
 * - gamedatnew
 */
void OldNew::RescanDir()
{
	lbl->setText("Scanning...");
	QCoreApplication::processEvents();

	//Init members (to their "nothing found" values)
	gamedatold = "";
	gamedatnew = "";

	QDir dir(".");

	if (!dir.exists()) {
		qWarning("The directory does not exist");
		return;
	}

	else
	{
		dir.setFilter(QDir::Files | QDir::AllDirs);
		dir.setSorting(QDir::Size | QDir::Reversed);

		QFileInfoList list = dir.entryInfoList();

		foreach (QFileInfo finfo, list)
		{
			// Only check files that match known game.dat sizes
			if (( finfo.size() == OLD_SIZE )  ||  ( finfo.size() == NEW_SIZE ))
			{
				QString name = finfo.fileName();
				QString checksum = fileSHA1(name);

				if(!(checksum.compare(NEW_SUM_SHA))) // is this the new game.dat?
					gamedatnew = name;

				if(!(checksum.compare(OLD_SUM_SHA))) // is this the old game.dat?
					gamedatold = name;
			}
		}
	}

	QTextStream out(stdout);
	out << "Found old gamedat: " << gamedatold << "\n";
	out << "Found new gamedat: " << gamedatnew << "\n";
}

void OldNew::ActNew()
{
	QString success = "New game.dat\n\nActive!";

	if (!(fileSHA1("./game.dat").compare(NEW_SUM_SHA))) {
		lbl->setText(success);
		return;
	}

	RescanDir();

	if (!(gamedatnew.compare(""))) {
		lbl->setText("No new game.dat \nfound...");
		return;
	}

	if (SwitchFiles("game.dat", gamedatnew))
		lbl->setText(success);
	else
		lbl->setText("Error: unable to\nmove file...");
}

void OldNew::ActOld()
{
	QString success = "Old game.dat\n\nActive!";

	if (!(fileSHA1("./game.dat").compare(OLD_SUM_SHA))) {
		lbl->setText(success);
		return;
	}

	RescanDir();

	if (!(gamedatold.compare(""))) {
		lbl->setText("No old game.dat \nfound...");
		return;
	}

	if (SwitchFiles("game.dat", gamedatold))
		lbl->setText(success);
	else
		lbl->setText("Error: unable to\nmove file...");
}

/**
 * Switches origfile and newfile.
 * 
 * NOTE: If origfile does not exist, newfile is still moved to origfile's specified location.
 *       = treated as success -> no warning
 * 
 * 
 * @return true if successful, false if not
 *      fail cases: 
 *        - unable to move origfile anywhere (permissions?)
 *        - absence of newfile
 */
bool OldNew::SwitchFiles(QString origfile, QString newfile)
{
	QFile newFile(newfile);
	if(!(newFile.exists())) // no newfile? don't bother then
		return false;

	QTextStream out(stdout);

	QFile currDat(origfile);
	QString number = "nowhere because it didn't exist!";

	// Perhaps there is no old game.dat. If so, we don't need to move it.
	if (currDat.exists())
	{
		int i = 0;

		// Keep trying to move game.dat to game<number>.off until we succeed
		// (this is a temporary location)
		while(i != -1)
		{
			i++;

			// we've tried 2000 numbers, apparently sth else is wrong.
			if(i > 2000)
				return false;

			number = QString(origfile).append(QString("%1.off").arg(i));

			bool moveOK = currDat.rename(number);

			// we moved succesfully! let's abort the loop then.
			if(moveOK)
				i = -1;
		}
	}

	// this should never fail because we just moved 'origfile' out of the way
	newFile.rename(origfile);

	// rename that ugly temporary file-name back to 'newfile' so it's really a switch action
	QFile moveBack(number);
	moveBack.rename(newfile);

	return true;
}


