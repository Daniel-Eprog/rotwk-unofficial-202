/*
 * ConfigParser.cpp
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "ConfigParser.h"

// For file handling
#include <QFile>
#include <QFileInfo>
#include <QDir>

#include <QDebug>

ConfigParser::ConfigParser()
{
	directory = ".";
}


ConfigParser::ConfigParser(QString dir)
{
	directory = dir;
}

QVector<QString> ConfigParser::getReleaseNames(int category)
{
	QVector<QString> foundReleases;

	QDir dir(directory);
	
	bool ok;

	if (!dir.exists()) {
		qWarning("The directory does not exist");
		return foundReleases;
	}

	else
	{
		dir.setFilter(QDir::Files);

		QFileInfoList list = dir.entryInfoList();

		foreach (QFileInfo finfo, list)
		{
			QSettings s(finfo.filePath(), QSettings::IniFormat);
			//~ s.setValue("animal/snake", 58);
			int val = s.value("information/category").toString().toInt(&ok, 10);

			if (!ok)
				val = -1;
				
			//~ qDebug() << QString("Category in file = <%1>").arg(val);
			//~ qDebug() << QString("Category should be = <%1>").arg(category);
			
			if( (finfo.suffix().compare(QString("ini")) == 0)  &&  ((val == category) || category == ALL_CATEGORIES) )
				foundReleases.push_front(finfo.completeBaseName());
		}
	}

	return foundReleases;
}

Release ConfigParser::createRelease(QString releaseName)
{
	Release release(releaseName);

	QString configLocation = directory + "/" + releaseName + ".ini";

	//~ qDebug() << configLocation;

	QSettings s(configLocation, QSettings::IniFormat);

	int size = s.beginReadArray("enableFiles");
	for (int i = 0; i < size; i++) {
		s.setArrayIndex(i);
		QString readName = s.value("name").toString();
		
		//if the file is not a dummy file, add it to the release.
		if (readName.compare(QString(DUMMY_FILE)) != 0) {
			release.addEnableFile(readName);
			release.addEnableFileChecksum(s.value("checksum").toString());
		}
	}
	s.endArray();

	size = s.beginReadArray("triggeredReleases");
	for (int i = 0; i < size; i++) {
		s.setArrayIndex(i);
		QString readName = s.value("name").toString();
		
		//if the release is not a dummy, add it
		if (readName.compare(QString(DUMMY_FILE)) != 0) { 
			Release trigger = createRelease(readName); 
			release.addTriggeredRelease(trigger);
		}
	}
	s.endArray();

	size = s.beginReadArray("disableFiles");
	for (int i = 0; i < size; i++) {
		s.setArrayIndex(i);
		QString readName = s.value("name").toString();
		
		//if the file is not a dummy file, add it to the release.
		if (readName.compare(QString(DUMMY_FILE)) != 0) 
			release.addDisableFile(readName);
	}
	s.endArray();

	bool ok;
	size = s.beginReadArray("switchFiles");
	for (int i = 0; i < size; i++) {
		s.setArrayIndex(i);

		QString name = s.value("name").toString();
		QString sum = s.value("checksum").toString();
		int val = s.value("maxMBfilesize").toString().toInt(&ok, 10);
		if(!ok)
			val = 50;

		SwitchFile sf(name, sum, val);

		release.addSwitchFile(sf);
	}
	s.endArray();


	return release;
}

