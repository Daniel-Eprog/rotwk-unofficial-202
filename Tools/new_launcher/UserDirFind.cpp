/*
 * UserDirFind.cpp
 *
 * Copyright 2017 Bram Bosch <bagbosch@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include "UserDirFind.h"

QString UserDirFind::AppdataDirFind()
{
	// This is a non-existent config file used to grab the "%appdata%/organization" folder on Windows.
	QSettings cfg(QSettings::IniFormat, QSettings::UserScope, "organization", "application");

	//strip the "/organization"
	return QFileInfo(cfg.fileName()).absolutePath() + "/../";
}

void UserDirFind::FindRotwkUserDirs(QString * dirs)
{
	QString config_dir = AppdataDirFind();
	QDir myDir = QDir(config_dir);

	// Find the name of the RotWK user directory (this directory might have a non-English name)
	QSettings settings("HKEY_LOCAL_MACHINE\\Software\\Electronic Arts\\Electronic Arts\\The Lord of the Rings, The Rise of the Witch-king",
				QSettings::NativeFormat);
	QString userDataLeaf = settings.value("UserDataLeafName", "My The Lord of the Rings, The Rise of the Witch-king Files").toString();

	QString optionsFile = myDir.absolutePath() + "/" + userDataLeaf;
	QString optionsFileB = myDir.absolutePath() + "/Roaming/" + userDataLeaf;

	dirs[0] = optionsFile;
	dirs[1] = optionsFileB;
}
