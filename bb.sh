#!/usr/bin/env bash

git diff -z --name-only origin/develop HEAD "$PWD/source/" ':(exclude)source/lang' ':(exclude)source/maps' | xargs --null cp --parents -t ~/Documents

git diff -z --name-only origin/develop HEAD "$PWD/ws/maps" | xargs --null cp --parents -t ~/Documents/source

mv ~/Documents/source/ws/maps  ~/Documents/source/

cd ~/Documents/source && bigtool -c ../\!beta.big * 
mv ~/Documents/\!beta.big "$1"
cd $PWD
rm -r ~/Documents/source
