# Modern UI example script
!include MUI.nsh
!include sections.nsh

Name "2.02 Christmas edition"
OutFile "202_christmas.exe"
InstallDirRegKey HKLM "Software\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king" InstallPath

SetCompressor /SOLID lzma

; Customization of the license page (so it says readme rather than license)
;!define MUI_PAGE_HEADER_TEXT "Read Me"
;!define MUI_PAGE_HEADER_SUBTEXT "For new players"
;!define MUI_LICENSEPAGE_TEXT_TOP "Please read the following information:"
;!define MUI_LICENSEPAGE_TEXT_BOTTOM " . "
;!define MUI_LICENSEPAGE_BUTTON "Next >"

; Customization of the finish page (checkbox for desktop schortcut)
;!define MUI_FINISHPAGE_SHOWREADME ""
;!define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
;!define MUI_FINISHPAGE_SHOWREADME_TEXT "Create Desktop Shortcut"
;!define MUI_FINISHPAGE_SHOWREADME_FUNCTION finishpageaction

;!define MUI_ICON "202icon.ico"
!define MUI_WELCOMEFINISHPAGE_BITMAP "welcomechristmas.bmp"

!insertmacro MUI_PAGE_WELCOME
;!insertmacro MUI_PAGE_LICENSE "license.rtf"
;!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

BrandingText "Rise of the Witch-King 2.02 -- www.gamereplays.org"


;;
; Main component
;;
Section
	;SectionIn RO ;Make it read-only
	SetOutPath $INSTDIR

	DetailPrint "Finding 2.02..."
		;Check if packaged bfme2 folder has not moved or sth
		${If} ${FileExists} "#########202_v7.0.0.big"
			DetailPrint "Good, 2.02 is present."
		${Else}
			DetailPrint "2.02 Not found!"
			MessageBox MB_OK|MB_ICONSTOP "2.02 files not found! Please install/enable 2.02 v7.0.0 first. "
			Abort
		${EndIf}

	File "#########_202ChristmasMod.big"
	File /r "launcher_releases"

	; Write the options.ini file for Win8 and Win10 users
	SetOutPath "$APPDATA\My The Lord of the Rings, The Rise of the Witch-king Files"
	SetOverwrite off
	File "Options.ini"
	SetOverwrite on

	; Reset back to regular install directory
	SetOutPath $INSTDIR

SectionEnd


Function .onInit
StrCpy $1 ${secws} ;The default
FunctionEnd


;;
; Make a desktop shortcut if the user asked for it
;;
Function finishpageaction
	CreateShortcut "$desktop\2.02 Switcher.lnk" "$INSTDIR\new_launcher.exe"
FunctionEnd


;;
; Helper function that removes the content of a directory
;;
!macro RemoveFilesAndSubDirs DIRECTORY
  !define Index_RemoveFilesAndSubDirs 'RemoveFilesAndSubDirs_{__LINE__}'

  Push $0
  Push $1
  Push $2

  StrCpy $2 "${DIRECTORY}"
  FindFirst $0 $1 "$2*.*"
${Index_RemoveFilesAndSubDirs}-loop:
  StrCmp $1 "" ${Index_RemoveFilesAndSubDirs}-done
  StrCmp $1 "." ${Index_RemoveFilesAndSubDirs}-next
  StrCmp $1 ".." ${Index_RemoveFilesAndSubDirs}-next
  IfFileExists "$2$1\*.*" ${Index_RemoveFilesAndSubDirs}-directory
  ; file
  Delete "$2$1"
  goto ${Index_RemoveFilesAndSubDirs}-next
${Index_RemoveFilesAndSubDirs}-directory:
  ; directory
  RMDir /r "$2$1"
${Index_RemoveFilesAndSubDirs}-next:
  FindNext $0 $1
  Goto ${Index_RemoveFilesAndSubDirs}-loop
${Index_RemoveFilesAndSubDirs}-done:

  FindClose $0

  Pop $2
  Pop $1
  Pop $0
  !undef Index_RemoveFilesAndSubDirs
!macroend



;;
; This spawns the confirmation window when uninstalling
;;
function un.onInit
	#Verify the uninstaller - last chance to back out
	MessageBox MB_OKCANCEL "Permanantly remove RotWK 2.02 version 7.0.0?" IDOK next
		Abort
	next:

functionEnd



